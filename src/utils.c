#include "utils.h"
#include "stdlib.h"
#include "time.h"
#ifdef _WIN32
	#include "windows.h"
#else
	#include "sys/stat.h"
	#include "utime.h"
	#include "unistd.h"
#endif
#ifdef __EMSCRIPTEN__
	#include <emscripten.h>
	#include <emscripten/html5.h>
#endif


int mod2(int a, int b){
	int r = a % b;
	return r < 0 ? r + b : r;
}

int Millisecs(){
	#ifdef _WIN32
		return (int)clock();
	#else
		struct timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		static unsigned long long int baseTime;
		unsigned long long int time = (unsigned long long int)ts.tv_sec*1000000000LLU + (unsigned long long int)ts.tv_nsec;
		if(!baseTime){
			baseTime = time;
			return 0;
		}else{
			return (int)((time - baseTime)*1e-9 * 1000);
		}
	#endif
}

void Delay(int ms){
	#ifdef _WIN32
		Sleep(ms);
	#elif defined(__EMSCRIPTEN__)
		return;
	#elif defined(__linux__)
		struct timespec req = { 0 };
		time_t sec = (int)(ms/1000.0f);
		ms -= (float)(sec*1000);
		req.tv_sec = sec;
		req.tv_nsec = ms*1000000L;

		while (nanosleep(&req, &req) == -1) continue;
	#elif defined(__APPLE__)
		usleep(ms*1000.0f);
	#endif
}

#ifdef __EMSCRIPTEN__

static EM_BOOL onEmscSizeChanged(int event_type, const EmscriptenUiEvent* ui_event, void* user_data){
	double w, h;
	float dpi_scale;
	emscripten_get_element_css_size("canvas", &w, &h);
	if(w < 1){
		w = ui_event->windowInnerWidth;
	}
	if(h < 1){
		h = ui_event->windowInnerHeight;
	}
	dpi_scale = emscripten_get_device_pixel_ratio();
	int width = (int)(w*dpi_scale);
	int height = (int)(h*dpi_scale);
	emscripten_set_canvas_element_size("canvas", width, height);

	EM_ASM_INT({
		GLFW.onCanvasResize($0, $1);
	}, width, height);
	return true;
}

void ApplyEmscriptenFixes(){
	//fix canvas resizing
	emscripten_set_resize_callback(EMSCRIPTEN_EVENT_TARGET_WINDOW, 0, false, onEmscSizeChanged);
}

#endif
