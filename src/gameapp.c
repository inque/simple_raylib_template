#include "gameapp.h"
#include "raymath.h"
#include "stdbool.h"
#include "time.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#ifdef __EMSCRIPTEN__
	#include <emscripten.h>
#endif


AppState_t app = {
	.configPath = "options.txt"
};
GameState_t game;
ClientState_t client = {.scale = 1};

void LoadAssets(){
	if(!app.headless){
		// ...
		// put your asset loading code here
		// ...
	}
}

void GameTick(){
	if(!app.headless){
		client.lastTickTime = GetTime();
		if(client.paused) return;
		if(client.screen != SCREEN_GAME) return;
	}

	if(app.shouldRestartGame){
		ResetGame();
		app.shouldRestartGame = 0;
	}
	// ...
	// update game logic here
	// e.g.: UpdateCharacters();
	// 
	// runs at a fixed tick rate, don't use delta time!
	// ...
	game.frame++;
}

void DrawGraphics(){
	if(app.headless) return;
	#ifdef __EMSCRIPTEN__
		client.dpi = emscripten_get_device_pixel_ratio();
	#else
		client.dpi = GetWindowScaleDPI().x;
	#endif
	client.width = GetScreenWidth();
	client.height = GetScreenHeight();
	client.halfWidth = client.width / 2;
	client.halfHeight = client.height / 2;
	client.cam.offset.x = (float)client.halfWidth;
	client.cam.offset.y = (float)client.halfHeight;
	client.time = (float)GetTime();
	client.deltaTime = GetFrameTime();
	client.deltaFrame = ((client.time - client.lastTickTime) * (float)GET_GAME_TICKRATE());
	
	client.scale = client.dpi;
	client.cam.zoom = client.scale;
	client.uiCam.zoom = client.scale;

	switch(client.screen){
		case SCREEN_MAINMENU: {
			// ...
			// you can replace this with your main menu screen
			// e.g.: DrawMenu(){
			//     if(ButtonClick){
			//         StartGame();
			//         client.screen = SCREEN_GAME;
			//     }
			// }
			// ...
			StartGame();
			client.screen = SCREEN_GAME;
		} break;
		case SCREEN_GAME: {
			BeginDrawing();
			ClearBackground(RAYWHITE);
			BeginMode2D(client.cam);
				// ...
				// draw game graphics here
				// e.g.: DrawCharacters();
				// 
				// use client.deltaFrame and such
				// or you'll depend on fixed tickrate!
				// ...
			EndMode2D();
			BeginMode2D(client.uiCam);
				DrawText("hello world", 150, 100, 24, BLACK);
			EndMode2D();
			EndDrawing();
		} break;
	}
}

void StartGame(){
	memset(&game, 0, sizeof(game));
	//...
	// add args to pass initial state here from the menu
	// (game mode, networking role, etc.)
	//...
	ResetGame();
}

void ResetGame(){
	game.state = STATE_UNINITIALIZED;
	game.frame = 0;
	game.timer = 0;

	if(!game.initialized){
		game.initialized = 1;
	}else{
		CloseGame();
	}

	if(!app.headless){
		client.cam = (Camera2D){
			.target = {0, 0},
			.rotation = 0,
			.zoom = 1
		};
		//...
		// set initial client UI state here
		//...
	}
}

void CloseGame(){
	//...
	// clean up the game board.
	// mind the mem leaks.
	//...
}
