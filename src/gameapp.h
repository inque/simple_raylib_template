#pragma once
#include "config.h"
#include "raylib.h"

#define GAME_TITLE "My Game"
#define TICKS_PER_SEC 60
#define GET_GAME_TICKRATE() app.config.tickrate

typedef enum{
	STATE_UNINITIALIZED,
	STATE_STARTING,
	STATE_PLAYING,
	STATE_ENDED
} GameState_e;

typedef struct{
	int initialized;
	int frame;
	int timer;
	GameState_e state;
} GameState_t;

typedef struct{
	char configPath[64];
	AppConfig_t config;
	int headless;
	int shouldRestartGame;
} AppState_t;

extern AppState_t app;
extern GameState_t game;

typedef enum{
	SCREEN_MAINMENU,
	SCREEN_GAME
} ClientScreen_e;

typedef struct{
	ClientScreen_e screen;
	int width, height;
	int halfWidth, halfHeight;
	float dpi, scale;
	float time, deltaTime;
	float lastTickTime, deltaFrame;
	Camera2D cam, uiCam;
	int shouldClose;
	int paused;
} ClientState_t;

extern ClientState_t client;

void LoadAssets();
void GameTick();
void DrawGraphics();
void StartGame();
void ResetGame();
void CloseGame();

#ifdef _MSC_VER
	//disable float conversion warnings
	#pragma warning(disable:4244 4305)
#endif
