#pragma once

typedef struct{
	int fullscreen;
	int vsync;
	int resolution[2];
	int windowPos[2];
	float scale;
	float soundVolume;
	float musicVolume;
	int tickrate;
} AppConfig_t;

void DefaultSettings();
void LoadSettings();
void SaveSettings();
