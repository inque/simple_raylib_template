#include "gameapp.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"

void DefaultSettings(){
	app.config = (AppConfig_t){
		.fullscreen = 0,
		.vsync = 0,
		.resolution = {1024, 720},
		.windowPos = {-1, -1},
		.scale = 1,
		.soundVolume = 0.725f,
		.musicVolume = 0.675f,
		.tickrate = TICKS_PER_SEC
	};
}

static void* FindCfgKey(char* cfg, size_t len, const char* key){
	size_t keyLen = strlen(key);
	size_t p = 0;
	while(p < len){
		if(!strncmp(key, cfg, keyLen)){
			while(p < len){
				p++; cfg++;
				if(cfg[-1] == '=') break;
			}
			size_t valLen = 0;
			while((p + valLen) < len){
				valLen++;
				if(cfg[valLen] == 0x0A) break;
				if(cfg[valLen] == 0)    break;
			}
			cfg[valLen] = 0;
			return (void*)cfg;
		}
		while(p < len){
			p++; cfg++;
			if(cfg[-1] == 0x0A) break;
			if(cfg[-1] == 0)   break;
		}
	}
	return NULL;
}

static void GetCfgOption_i(char* cfg, int len, const char* key, int* out){
	void* val = FindCfgKey(cfg, len, key);
	if(val) *out = atoi(val);
}

#ifdef __EMSCRIPTEN__
	EMSCRIPTEN_KEEPALIVE
#endif
void LoadSettings(){
	DefaultSettings();
	
	#ifdef __EMSCRIPTEN__
		strcpy(app.configPath, "/mygame/config.txt");
		EM_ASM(
			if(Module.syncdone != 1){
				FS.mkdir('/mygame');
				FS.mount(IDBFS, {}, '/mygame');
				Module.syncdone = 0;
				FS.syncfs(true, function(err){
					if(err) console.error(err);
					Module.syncdone = 1;
					Module.ccall('LoadSettings', null, null, null);
				});
			}
		);
	#endif

	FILE* file = fopen(app.configPath, "r");
	if(file){
		int scale = 100;
		int volume = 70;
		char cfg[8192];
		size_t len = fread(cfg, 1, sizeof(cfg)-1, file);
		cfg[len] = 0;
		fclose(file);

		GetCfgOption_i(cfg, len, "fullscreen",  &app.config.fullscreen);
		GetCfgOption_i(cfg, len, "vsync",       &app.config.vsync);
		GetCfgOption_i(cfg, len, "width",       &app.config.resolution[0]);
		GetCfgOption_i(cfg, len, "height",      &app.config.resolution[1]);
		GetCfgOption_i(cfg, len, "windowpos_x", &app.config.windowPos[0]);
		GetCfgOption_i(cfg, len, "windowpos_y", &app.config.windowPos[1]);
		GetCfgOption_i(cfg, len, "scale", &scale);
		app.config.scale = scale/100.0f;
		GetCfgOption_i(cfg, len, "sound_volume", &volume);
		app.config.soundVolume = volume/100.0f;
		GetCfgOption_i(cfg, len, "music_volume", &volume);
		app.config.musicVolume = volume/100.0f;
		GetCfgOption_i(cfg, len, "gamespeed",   &app.config.tickrate);
	}
}

static void WriteCfgOption_i(const char* key, int value, FILE* file){
	char line[1024];
	int len = sprintf(line, "%s=%i\n", key, value);
	fwrite(line, 1, len, file);
}

void SaveSettings(){
	FILE* file = fopen(app.configPath, "w");
	if(!app.config.fullscreen){
		Vector2 windowPos = GetWindowPosition();
		app.config.windowPos[0] = (int)windowPos.x;
		app.config.windowPos[1] = (int)windowPos.y;
		app.config.resolution[0] = GetScreenWidth();
		app.config.resolution[1] = GetScreenHeight();
	}
	if(file){
		WriteCfgOption_i("fullscreen",  app.config.fullscreen,    file);
		WriteCfgOption_i("vsync",       app.config.vsync,         file);
		WriteCfgOption_i("width",       app.config.resolution[0], file);
		WriteCfgOption_i("height",      app.config.resolution[1], file);
		WriteCfgOption_i("windowpos_x", app.config.windowPos[0],  file);
		WriteCfgOption_i("windowpos_y", app.config.windowPos[1],  file);
		WriteCfgOption_i("scale",        (int)(app.config.scale*100), file);
		WriteCfgOption_i("sound_volume", (int)(app.config.soundVolume*100), file);
		WriteCfgOption_i("music_volume", (int)(app.config.musicVolume*100), file);
		WriteCfgOption_i("gamespeed",   app.config.tickrate, file);
		fclose(file);
	}

	#ifdef __EMSCRIPTEN__
		EM_ASM(
			FS.syncfs(false, function(err){if(err) console.error(err);} );
		);
	#endif
}
