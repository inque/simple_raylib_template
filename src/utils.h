#pragma once
#include "stdint.h"

//modulo operator, but no negative numbers
int mod2(int a, int b);

//current time in milliseconds
int Millisecs();

//sleep
void Delay(int ms);

#ifdef __EMSCRIPTEN__
    void ApplyEmscriptenFixes();
#endif
